﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TohaAPI
{
    public class Content
    {
        public String ContentString { get; set; }
        public int Type { get; set; }
    }
}
