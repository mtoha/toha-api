﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TohaAPI
{
    public class TohaAPI
    {
        private static Dictionary<string, Content> dictionary = new Dictionary<string, Content>();

        public void Register(string itemName, string itemContent, int itemType)
        {
            if (itemType == 1)
            {
                if (itemContent.Contains("{") && itemContent.Contains("}"))
                {
                    Content c = new Content();
                    c.ContentString = itemContent;
                    c.Type = itemType;
                    dictionary.Add(itemName, c);
                }
                else
                {
                    Console.WriteLine("Make sure your content is corrent json format");
                }
            }
            else if (itemType == 2)
            {
                if (itemContent.Contains("</") || itemContent.Contains("/>"))
                {
                    Content c = new Content();
                    c.ContentString = itemContent;
                    c.Type = itemType;
                    dictionary.Add(itemName, c);
                }
                else
                {
                    Console.WriteLine("Make sure your content is corrent XML format");
                }
            }
        }

        public string Retrieve(string itemName)
        {
            Content c = new Content();
            dictionary.TryGetValue(itemName, out c);
            return c == null ? null : c.ContentString;
        }

        public int? GetType(string itemName)
        {
            Content c = new Content();
            dictionary.TryGetValue(itemName, out c);
            if (c == null) return null;
            else return c.Type;
        }

        public void Deregister(string itemName)
        {
            dictionary.Remove(itemName);
        }

    }
}
